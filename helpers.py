from functools import partial
from pysc2.lib import actions, units

def get_units(obs, unit_type, predicate=lambda _: True):
    return [unit for unit in obs.observation.feature_units 
                if unit.unit_type == unit_type and predicate(unit)]


def get_seen_units(obs, unit_type, predicate=lambda _: True):
    def seen_predicate(predicate, unit):
        return predicate(unit) and unit.x > 0 and unit.y > 0
    return get_units(obs, unit_type, partial(seen_predicate, predicate))


def get_mains(obs, predicate=lambda _: True):
    return get_units(obs, units.Zerg.Hatchery, predicate=predicate)\
        + get_units(obs, units.Zerg.Lair, predicate=predicate)\
        + get_units(obs, units.Zerg.Hive, predicate=predicate)


def is_unit_type_selected(obs, unit_type):
    if (len(obs.observation.single_select) > 0 and
                obs.observation.single_select[0].unit_type == unit_type):
            return True

    if (len(obs.observation.multi_select) > 0 and
            any(select.unit_type == unit_type for select in obs.observation.multi_select)):
        return True

    return False


def is_action_available(obs, action):
    return action.id in obs.observation.available_actions