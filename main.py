from pysc2.agents import base_agent
from pysc2.env import sc2_env
from pysc2.lib import actions, features
from absl import app

from action_factories import StrategyBasedActionFactory
from strategies import RoachRushStrategy
from logger import get_logger


class IntelligentAgent(base_agent.BaseAgent):
    def __init__(self, command_factory):
        super().__init__()
        self._command_factory = command_factory
    
    def reset(self):
        super().reset()
        self._command_factory.reset()

    def step(self, obs):
        return self._command_factory.get_action(obs)


def main(_):
    agent = IntelligentAgent(StrategyBasedActionFactory(RoachRushStrategy()))
    try:
        while True:
            with sc2_env.SC2Env(
                    map_name="Acropolis",
                    players=[sc2_env.Agent(sc2_env.Race.zerg),
                             sc2_env.Bot(sc2_env.Race.protoss,
                                         sc2_env.Difficulty.easy)],
                    agent_interface_format=features.AgentInterfaceFormat(
                        feature_dimensions=features.Dimensions(screen=84, minimap=64),
                        use_feature_units=True,
                        show_placeholders=True),
                    step_mul=8,
                    game_steps_per_episode=0,
                    visualize=True) as env:

                agent.setup(env.observation_spec(), env.action_spec())

                timesteps = env.reset()
                agent.reset()

                while True:
                    step_actions = [agent.step(timesteps[0])]
                    if timesteps[0].last():
                        break
                    timesteps = env.step(step_actions)

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    app.run(main)
