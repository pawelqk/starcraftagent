from pysc2.lib import actions

from .action import Action


class AttackOnScreen(Action):
    def __init__(self, attack_coords):
        self._attack_coords = attack_coords

    def get(self, _):
        return actions.FUNCTIONS.Attack_screen('now', self._attack_coords)
