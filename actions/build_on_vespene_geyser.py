import random
from pysc2.lib import actions
from pysc2.lib.units import Neutral

from .build import Build
from helpers import get_seen_units


class BuildOnVespeneGeyser(Build):
    def __init__(self):
        super().__init__(actions.FUNCTIONS.Build_Extractor_screen)

    def find_free_coords(self, obs):
        y, x = (obs.observation.feature_screen.unit_type == Neutral.SpacePlatformGeyser).nonzero()
        if len(y) == 0 or len(x) == 0:
            return None

        i = random.randint(0, len(y) - 1)
        return x[i], y[i]
