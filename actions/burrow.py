from pysc2.lib import actions

from .action import Action
from helpers import is_action_available


class Burrow(Action):
    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.BurrowDown_quick):
            return actions.FUNCTIONS.BurrowDown_quick('now')
        else:
            return actions.FUNCTIONS.no_op()


class Unburrow(Action):    
    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.BurrowUp_quick):
            return actions.FUNCTIONS.BurrowUp_quick('now')
        else:
            return actions.FUNCTIONS.no_op()
