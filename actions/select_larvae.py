from pysc2.lib import actions

from .action import Action


class SelectLarvae(Action):
    def get(self, _):
        return actions.FUNCTIONS.select_larva()
