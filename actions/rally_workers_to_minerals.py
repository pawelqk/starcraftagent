from pysc2.lib import actions, units
import random

from .action import Action
from helpers import is_action_available, get_seen_units
from logger import get_logger


class RallyWorkersToMinerals(Action):
    def __init__(self):
        self._logger = get_logger(__name__)

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Rally_Workers_screen):
            return actions.FUNCTIONS.Rally_Workers_screen('now', self._find_minerals_coords(obs))
        else:
            self._logger.warning('Couldnt rally units to minimap for some reason')
            return actions.FUNCTIONS.no_op()

    @staticmethod
    def _find_minerals_coords(obs):
        minerals = get_seen_units(obs, units.Neutral.LabMineralField)
        if len(minerals) == 0:
            minerals = get_seen_units(obs, units.Neutral.LabMineralField750)
        
        mineral = random.choice(minerals)
        return mineral.x, mineral.y