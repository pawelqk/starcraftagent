from pysc2.lib import actions

from .action import Action
from helpers import is_action_available
from logger import get_logger


class RallyUnitOnMinimap(Action):
    def __init__(self, rally_coords):
        self._rally_coords = rally_coords
        self._logger = get_logger(__name__)

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Rally_Units_minimap):
            return actions.FUNCTIONS.Rally_Units_minimap('now', self._rally_coords)
        else:
            self._logger.warning('Couldnt rally units on minimap for some reason')
            return actions.FUNCTIONS.no_op()