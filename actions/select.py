from functools import partial
import random
from pysc2.lib import actions, units

from logger import get_logger
from helpers import get_seen_units, is_unit_type_selected
from .action import Action, ActionException


class SelectFromAll(Action):
    def __init__(self, unit_type, selection_type, predicate):
        if not isinstance(unit_type, units.Zerg) or not callable(predicate):
            raise ActionException('Select got wrong types of unit_source and predicate')

        self._unit_type = unit_type
        self._selection_type = selection_type
        self._predicate = predicate
        self._logger = get_logger(f'{selection_type}({unit_type})')
    
    def __str__(self):
        return 'SelectFromAll ' + str(self._unit_type)

    def get(self, obs):
        self._logger.debug(self._unit_type)
        units_seen = get_seen_units(obs, self._unit_type, self._predicate)
        if len(units_seen) == 0:
            self._logger.warning('Cant find unit of type ' + str(self._unit_type) + 'The action will be repeated')
            return actions.FUNCTIONS.no_op()

        unit = random.choice(units_seen)
        return actions.FUNCTIONS.select_point(self._selection_type, (unit.x, unit.y))


class SelectAllType(SelectFromAll):
    def __init__(self, unit_type, predicate=lambda _: True):
        super().__init__(unit_type, 'select_all_type', predicate)
        self._try_counter = 0
    
    def get_callback(self):
        def callback(unit_type, obs):
            selection_succeeded = is_unit_type_selected(obs, unit_type)
            if not selection_succeeded:
                self._try_counter += 1
            if self._try_counter == 20:
                return True
            
            return selection_succeeded

        return partial(callback, self._unit_type)


class SelectRandom(SelectFromAll):
    def __init__(self, unit_type, predicate=lambda _: True):
        super().__init__(unit_type, 'select', predicate)

    def get_callback(self):
        def callback(unit_type, obs):
            return len(obs.observation.single_select) > 0 and obs.observation.single_select[0].unit_type == unit_type

        return partial(callback, self._unit_type)


class SelectSingle(Action):
    def __init__(self, unit_type, unit_coords):
        self._unit_coords = unit_coords
        self._unit_type = unit_type
        self._try_counter = 0
    
    def get(self, _):
        return actions.FUNCTIONS.select_point('select', self._unit_coords)
    
    def get_callback(self):
        def callback(unit_type, obs):
            selection_succeeded = len(obs.observation.single_select) > 0 and obs.observation.single_select[0].unit_type == unit_type
            if not selection_succeeded:
                self._try_counter += 1
            if self._try_counter == 5:
                return True
            
            return selection_succeeded

        return partial(callback, self._unit_type)

class SelectRandomFromScreen(Action):
    def __init__(self, unit_type):
        if not isinstance(unit_type, units.Zerg):
            raise ActionException('SelectRandomFromScreen got wrong types of unit_source and predicate')

        self._unit_type = unit_type
        self._logger = get_logger('SelectRandomFromScreen')
    
    def __str__(self):
        return 'SelectFromAll ' + str(self._unit_type)

    def get(self, obs):
        self._logger.debug(self._unit_type)
        y, x = (obs.observation.feature_screen.unit_type == self._unit_type).nonzero()
        if len(y) == 0:
            self._logger.warning('Cant find unit of type ' + str(self._unit_type) + 'The action will be repeated')
            return actions.FUNCTIONS.no_op()

        i = random.randint(0, len(y) - 1)
        return actions.FUNCTIONS.select_point('select', (x[i], y[i]))

    def get_callback(self):        
        return partial(is_unit_type_selected, unit_type=self._unit_type)