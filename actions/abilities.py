from pysc2.lib import actions

from .action import Action

from helpers import is_action_available

class CorrosiveBile(Action):
    def __init__(self, coords):
        self._coords = coords

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Effect_CorrosiveBile_screen):
            return actions.FUNCTIONS.Effect_CorrosiveBile_screen('now', self._coords)
        else:
            return actions.FUNCTIONS.no_op()
