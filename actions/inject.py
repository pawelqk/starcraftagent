from pysc2.lib import actions, units
import random

from .action import Action
from helpers import get_mains, is_action_available


class Inject(Action):
    def __init__(self, main_coords):
        self._main_coords = main_coords

    def get(self, obs):
        units_seen = get_mains(obs, predicate=lambda main: main.buff_id_0 != 11 and main.buff_id_1 != 11)
        if is_action_available(obs, actions.FUNCTIONS.Effect_InjectLarva_screen):
            unit = random.choice(units_seen)
            return actions.FUNCTIONS.Effect_InjectLarva_screen('now', (unit.x, unit.y))
        else:
            return actions.FUNCTIONS.no_op()
