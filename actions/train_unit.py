from pysc2.lib import actions, units

from .action import Action, ActionException
from helpers import is_action_available, is_unit_type_selected
from logger import get_logger

class TrainUnit(Action):
    def __init__(self, unit_source, training_function):
        if not isinstance(unit_source, units.Zerg) or not isinstance(training_function, actions.Function):
            raise ActionException('TrainUnit got wrong types of unit_source or training_function')

        self._unit_source = unit_source
        self._training_function = training_function
        self._logger = get_logger('TrainUnit ' + str(self._training_function) + str(self._unit_source))

    def get(self, obs):
        if is_unit_type_selected(obs, self._unit_source):
            self._logger.debug(obs.observation.available_actions)
            if is_action_available(obs, self._training_function):
                return self._training_function('now')
            else:
                self._logger.warning('Can not train that unit right now for some reason')
                return actions.FUNCTIONS.no_op()
        else:
            self._logger.error('unit has not been selected before. this should NOT happen')