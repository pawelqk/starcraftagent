from functools import partial
import random
import numpy as np

from pysc2.lib import actions, units

from helpers import is_action_available, is_unit_type_selected
from logger import get_logger
from .action import Action, ActionException


class Build(Action):
    def __init__(self, building_function):
        if not isinstance(building_function, actions.Function):
            raise ActionException('Build command got wrong type of building_function')
        self._building_function = building_function
        self._logger = get_logger(__name__)
        self._placeholder_count = 0
    
    def __str__(self):
        return 'Build of ' + str(self._building_function)
    
    @staticmethod
    def find_free_coords(obs):
        units_on_screen = obs.observation.feature_screen.unit_type
        mining_y, mining_x = ((units_on_screen == units.Neutral.LabMineralField) |\
            (units_on_screen == units.Neutral.LabMineralField750) |\
            (units_on_screen == units.Neutral.SpacePlatformGeyser) |\
            (units_on_screen == units.Zerg.Extractor)).nonzero()
        creep_y, creep_x = obs.observation.feature_screen.creep.nonzero()
        min_y = min(mining_y)
        max_y = max(mining_y)
        min_x = min(mining_x)
        max_x = max(mining_x)

        proper = False
        while not proper:
            rand_x, rand_y = random.randint(min(creep_x), max(creep_x)), random.randint(min(creep_y), max(creep_y))
            proper = rand_x < min_x or rand_x > max_x or rand_y < min_y or rand_y > max_y

        return rand_x, rand_y

    def get(self, obs):
        if is_unit_type_selected(obs, units.Zerg.Drone):
            if is_action_available(obs, self._building_function):
                build_coords = self.find_free_coords(obs)
                if build_coords:
                    self._placeholder_count = np.count_nonzero(obs.observation.feature_screen.placeholder)
                    return self._building_function("now", build_coords)
                else:
                    self._logger.warning('could not find free coords. will not build')
                    return actions.FUNCTIONS.no_op()
            else:
                self._logger.warning('Can not build right now for some reason')
                return actions.FUNCTIONS.no_op()
        else:
            self._logger.error('Drone has not been selected before. this should NOT happen')
            return actions.FUNCTIONS.no_op()

    def get_callback(self):
        def callback(placeholder_count, obs):
            return np.count_nonzero(obs.observation.feature_screen.placeholder) != placeholder_count

        return partial(callback, self._placeholder_count)
