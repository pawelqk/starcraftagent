from pysc2.lib import actions

from .action import Action


class CenterScreen(Action):
    def __init__(self, coords):
        self._coords = coords

    def get(self, _):
        return actions.FUNCTIONS.move_camera(self._coords)


class CenterScreenOnSelected(Action):
    def get(self, obs):
        coords = self._get_coords_of_selection(obs)
        if coords is not None:
            return actions.FUNCTIONS.move_camera(coords)
        else:
            return actions.FUNCTIONS.no_op()
    
    @staticmethod
    def _get_coords_of_selection(obs):
        y, x = obs.observation.feature_minimap.selected.nonzero()
        return (x.mean(), y.mean()) if x.size > 0 and y.size > 0 else None