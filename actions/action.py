from abc import ABC, abstractmethod


class ActionException(Exception):
    pass


class Action(ABC):
    @abstractmethod
    def get(self, obs):
        pass

    def get_callback(self):  # pylint: disable=R0201
        return lambda _: True
