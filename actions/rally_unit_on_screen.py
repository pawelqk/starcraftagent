from pysc2.lib import actions

from .action import Action, ActionException


class RallyUnitOnScreen(Action):
    def __init__(self, coords_callback):
        if not callable(coords_callback):
            raise ActionException('RallyUnitOnScreen requires callback to receive rally coords')
        self._coords_callback = coords_callback

    def get(self, obs):
        return actions.FUNCTIONS.Rally_Units_screen('now', self._coords_callback(obs))
