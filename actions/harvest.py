from functools import reduce
import random
from pysc2.lib import actions

from helpers import is_action_available, get_seen_units
from .action import Action, ActionException


class HarvestFromNotFull(Action):
    def __init__(self, field_types):
        if type(field_types) is not tuple:
            raise ActionException('HarvestFromNotFull requires tuple of field types as input')
        self._field_types = field_types

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Harvest_Gather_screen):
            coords = self._get_not_full_field_coords(obs)
            if coords is not None:
                return actions.FUNCTIONS.Harvest_Gather_screen('now', coords)

        return actions.FUNCTIONS.no_op()

    def _get_not_full_field_coords(self, obs):
        def not_full_predicate(field):
            # for whatever reason, the API doesn't give information about count of harvesters
            # for mineral fields, therefore you can't determine which one is not full
            # but it's not a problem since the game will automatically place the drone where it's needed
            return field.assigned_harvesters < field.ideal_harvesters if field.ideal_harvesters != 0 else True

        fields = reduce(lambda a, b: a + b, [get_seen_units(obs, field_type, not_full_predicate) for field_type in self._field_types])
        field = random.choice(fields) if len(fields) > 0 else None

        if field is not None:
            return field.x, field.y
