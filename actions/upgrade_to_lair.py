from pysc2.lib import actions

from .action import Action


class UpgradeToLair(Action):
    def get(self, _):
        return actions.FUNCTIONS.Morph_Lair_quick('now')
