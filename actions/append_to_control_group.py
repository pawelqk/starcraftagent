from pysc2.lib import actions

from .action import Action


class AppendToControlGroup(Action):
    def __init__(self, control_group_id):
        self._control_group_id = control_group_id

    def get(self, _):
        return actions.FUNCTIONS.select_control_group('append', self._control_group_id)
