from pysc2.lib import actions

from .action import Action
from helpers import is_action_available
from logger import get_logger


class MoveOnScreen(Action):
    def __init__(self, coords):
        self._coords = coords
        self._logger = get_logger(__name__)
    
    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Move_screen):
            return actions.FUNCTIONS.Move_screen('now', self._coords)
        else:
            self._logger.warning('Cannot move the unit right now')
            return actions.FUNCTIONS.no_op()
