from math import floor
import random
from pysc2.lib import actions, units

from .build import Build
from helpers import get_seen_units


class BuildExpansion(Build):
    def __init__(self, expansion_coords):
        super().__init__(actions.FUNCTIONS.Build_Hatchery_screen)
        self._expansion_coords = expansion_coords

    def find_free_coords(self, obs):
        units_on_screen = obs.observation.feature_screen.unit_type
        mining_y, mining_x = (
            (units_on_screen == units.Neutral.LabMineralField) |\
            (units_on_screen == units.Neutral.LabMineralField750)).nonzero()

        if self._expansion_coords[0] > 32:
            limit_x = floor(min(mining_x))
            limit_y = floor(min(mining_y))
        else:
            limit_x = floor(max(mining_x))
            limit_y = floor(max(mining_y))

        return random.randint(limit_x - 7, limit_x + 8), random.randint(limit_y - 7, limit_y + 8)
