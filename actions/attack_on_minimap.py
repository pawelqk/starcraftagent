from pysc2.lib import actions

from .action import Action

from helpers import is_action_available


class AttackOnMinimap(Action):
    def __init__(self, attack_coords):
        self._attack_coords = attack_coords

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Attack_minimap):
            return actions.FUNCTIONS.Attack_minimap('now', self._attack_coords)
        else:
            return actions.FUNCTIONS.no_op()
