from pysc2.lib import actions

from .action import Action

from helpers import is_action_available


class SelectIdleWorkers(Action):
    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.select_idle_worker):
            return actions.FUNCTIONS.select_idle_worker('select_all')
        else:
            return actions.FUNCTIONS.no_op()
