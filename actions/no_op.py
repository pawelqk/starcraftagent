from pysc2.lib import actions

from .action import Action


class NoOp(Action):
    def get(self, _):
        return actions.FUNCTIONS.no_op()
