from pysc2.lib import actions

from .action import Action
from helpers import is_action_available
from logger import get_logger


class MorphRavager(Action):
    def __init__(self):
        self._logger = get_logger(__name__)
    
    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Morph_Ravager_quick):
            return actions.FUNCTIONS.Morph_Ravager_quick('now')
        else:
            self._logger.warning('Cannot morph ravager right now')
            return actions.FUNCTIONS.no_op()


class MorphOverseer(Action):
    def __init__(self):
        self._logger = get_logger(__name__)

    def get(self, obs):
        if is_action_available(obs, actions.FUNCTIONS.Morph_Overseer_quick):
            return actions.FUNCTIONS.Morph_Overseer_quick('now')
        else:
            self._logger.warning('Cannot morph overseer right now')
            return actions.FUNCTIONS.no_op()
