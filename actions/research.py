from pysc2.lib import actions

from .action import Action


class ResearchBurrow(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_Burrow_quick('now')


class ResearchPneumatizedCarapace(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_PneumatizedCarapace_quick('now')


class ResearchGroundArmor(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_ZergGroundArmor_quick('now')


class ResearchMissile(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_ZergMissileWeapons_quick('now')


class ResearchMuscularAugments(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_MuscularAugments_quick('now')


class ResearchGroovedSpines(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_GroovedSpines_quick('now')


class ResearchGlialReconstitution(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_GlialRegeneration_quick('now')


class ResearchTunnelingClaws(Action):
    def get(self, _):
        return actions.FUNCTIONS.Research_TunnelingClaws_quick('now')
