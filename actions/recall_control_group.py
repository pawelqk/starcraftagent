from pysc2.lib import actions

from .action import Action


class RecallControlGroup(Action):
    def __init__(self, control_group_id):
        self._control_group_id = control_group_id

    def get(self, _):
        return actions.FUNCTIONS.select_control_group('recall', self._control_group_id)
