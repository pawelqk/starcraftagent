import logging

HANDLER = logging.FileHandler(filename='log.txt', mode='w')
HANDLER.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s [%(name)s] %(message)s'))


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(HANDLER)
    logger.propagate = False

    return logger
