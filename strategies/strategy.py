from abc import ABC, abstractmethod

class Strategy(ABC):
    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def notify(self, action_results, alerts):
        pass
    
    @abstractmethod
    def create_command(self, obs):
        pass