from enum import Enum
from math import floor
import random
from queue import Queue
from .controller import ControllerState, Controller
from commands import CenterScreenOnMinimap, RecallControlGroupCommand,\
    ResearchMuscularAugmentsInHydraliskDen, ResearchGroovedSpinesInHydraliskDen,\
    ResearchGlialReconstitutionInRoachWarren, ResearchTunnelingClawsInRoachWarren, ResearchBurrowCommand, ResearchPneumatizedCarapaceCommand,\
    ResearchMissileCommand, ResearchGroundArmorCommand
from pysc2.lib import actions, features, units

from logger import get_logger
from helpers import is_action_available, is_unit_type_selected


class UpgradePlace(Enum):
    BUILDINGS = 0,
    MAIN = 1,
    EVOLUTION_CHAMBER = 2


class UpgradeController(Controller):
    def __init__(self, minimap_coords, main_group_id, evolution_chamber_group_id, indication_storage):
        super().__init__(indication_storage)
        self._minimap_coords = minimap_coords
        self._main_group_id = main_group_id
        self._evolution_chamber_group_id = evolution_chamber_group_id
        self._logger = get_logger('UpgradeController')
        
        self._upgrade_place = UpgradePlace.BUILDINGS
        self._missile_level = 0
        self._ground_level = 0
        self._evolution_level = 1
        self._upgrades_from_buildings = {
            'Muscular Augments': {
                'minerals': 100,
                'vespene': 100,
                'building': units.Zerg.HydraliskDen,
                'command': ResearchMuscularAugmentsInHydraliskDen
            },
            'Grooved Spines': {
                'minerals': 100,
                'vespene': 100,
                'building': units.Zerg.HydraliskDen,
                'command': ResearchGroovedSpinesInHydraliskDen
            },
            'Glial Reconstitution': {
                'minerals': 100,
                'vespene': 100,
                'building': units.Zerg.RoachWarren,
                'command': ResearchGlialReconstitutionInRoachWarren
            },
            'Tunneling Claws': {
                'minerals': 150,
                'vespene': 150,
                'building': units.Zerg.RoachWarren,
                'command': ResearchGlialReconstitutionInRoachWarren
            }
        }
        self._upgrades_from_main = {
            'Burrow': {
                'minerals': 100,
                'vespene': 100,
                'command': ResearchBurrowCommand
            },
            'Pneumatized Carapace': {
                'minerals': 75,
                'vespene': 75,
                'command': ResearchPneumatizedCarapaceCommand
            }
        }

    def prepare(self):
        if self._upgrade_place == UpgradePlace.BUILDINGS:
            self._preparation = CenterScreenOnMinimap(self._minimap_coords)
        elif self._upgrade_place == UpgradePlace.MAIN:
            self._preparation = RecallControlGroupCommand(self._main_group_id)
        elif self._upgrade_place == UpgradePlace.EVOLUTION_CHAMBER:
            self._preparation = RecallControlGroupCommand(self._evolution_chamber_group_id)

        return super().prepare()

    def get_commands(self, obs):
        queue = Queue()
        self.state = ControllerState.ACTIVE
        if self._upgrade_place == UpgradePlace.BUILDINGS:
            self._check_upgrades_from_buildings(queue, obs)
            self._upgrade_place = UpgradePlace.MAIN
        elif self._upgrade_place == UpgradePlace.MAIN:
            self._check_upgrades_from_main(queue, obs)
            self._upgrade_place = UpgradePlace.EVOLUTION_CHAMBER
        elif self._upgrade_place == UpgradePlace.EVOLUTION_CHAMBER:
            self._check_upgrades_from_evolution_chamber(queue, obs)
            self._upgrade_place = UpgradePlace.BUILDINGS

        return queue

    def _check_upgrades_from_buildings(self, queue, obs):
        minerals, vespene, _, _ = self._extract_info(obs)
        if self._evolution_level < 2:
            return
        done_list = []
        for name in self._upgrades_from_buildings:
            building = self._upgrades_from_buildings.get(name)['building']
            y, x = (obs.observation.feature_screen.unit_type == building).nonzero()
            if len(y) > 0 and len(x) > 0 and not self._is_building_type_ongoing(obs, building):
                needed_minerals = self._upgrades_from_buildings.get(name)['minerals']
                needed_vespene = self._upgrades_from_buildings.get(name)['vespene']  # TODO: this wont work if the building is still ongoing...
                if minerals >= needed_minerals and vespene >= needed_vespene:
                    minerals -= needed_minerals
                    vespene -= needed_vespene
                    queue.put(self._upgrades_from_buildings.get(name)['command']())
                    done_list.append(name)
        
        for name in done_list:
            self._upgrades_from_buildings.pop(name)

    def _check_upgrades_from_main(self, queue, obs):
        minerals, vespene, _, _ = self._extract_info(obs)
        self._check_main_level(obs)
        done_list = []
        for name in self._upgrades_from_main:
            needed_minerals = self._upgrades_from_main.get(name)['minerals']
            needed_vespene = self._upgrades_from_main.get(name)['vespene']
            if minerals >= needed_minerals and vespene >= needed_vespene:
                minerals -= needed_minerals
                vespene -= needed_vespene
                queue.put(self._upgrades_from_main.get(name)['command']())
                done_list.append(name)
        
        for name in done_list:
            self._upgrades_from_main.pop(name)    


    def _check_upgrades_from_evolution_chamber(self, queue, obs):
        minerals, vespene, _, _ = self._extract_info(obs)
        if self._missile_level == self._evolution_level and\
            self._ground_level == self._evolution_level:
            return

        if self._missile_level > self._ground_level:
            needed_minerals = 100 + 50 * self._ground_level
            needed_vespene = needed_minerals
            if is_action_available(obs, actions.FUNCTIONS.Research_ZergMissileWeapons_quick) and\
                minerals >= needed_minerals and vespene >= needed_vespene:
                queue.put(ResearchGroundArmorCommand())
                self._logger.info('researching armor')
                self._ground_level += 1
        else:
            needed_minerals = 100 + 50 * self._missile_level
            needed_vespene = needed_minerals
            if is_action_available(obs, actions.FUNCTIONS.Research_ZergGroundArmor_quick) and\
                minerals >= needed_minerals and vespene >= needed_vespene:
                queue.put(ResearchMissileCommand())
                self._logger.info('researching attack')
                self._missile_level += 1
    
    def _check_main_level(self, obs):
        if is_unit_type_selected(obs, units.Zerg.Lair):
            self._evolution_level = 2
    
    def are_all_upgrades_done_in_building(self, building):
        return len([
            upgrade for upgrade in self._upgrades_from_buildings if upgrade['building'] == building
        ]) == 0