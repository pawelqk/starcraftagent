from math import ceil
import numpy as np
from queue import Queue
import random
from .controller import ControllerState, Controller
from commands import AppendAllToControlGroup, AttackWithUnits, CenterScreenOnControlGroup, CenterScreenOnMinimap,\
    ControlGroupAttackOnMinimap, ControlGroupAttackOnScreen,  ControlGroupRetreatOnMinimap,\
    CorrosiveBileCommand, MoveUnit, SetAllAsControlGroup, BurrowUnit, UnburrowUnit, ControlGroupMoveOnScreen
from pysc2.lib import actions, features, units

from logger import get_logger
from helpers import is_action_available, get_units, get_seen_units

# basing on https://github.com/Blizzard/s2client-proto/blob/master/s2clientprotocol/sc2api.proto
BUILDING_UNDER_ATTACK = 6
UNIT_UNDER_ATTACK = 19

RETREAT_RANGE = 5


class ArmyController(Controller):
    def __init__(self, army_rally_minimap_coords, enemy_minimap_coords, reinforcer, indication_storage):
        super().__init__(indication_storage)
        self.army_rally_minimap_coords = army_rally_minimap_coords
        self._enemy_minimap_coords = enemy_minimap_coords
        self._reinforcer = reinforcer
        self._logger = get_logger('ArmyController')

        self._attacking = False  # mb not needed
        self._retreating = False
        self._logger.info('created')
        self.army_size = 6
        self.burrow = False

    def prepare(self):
        if self._attacking:
            self._preparation = CenterScreenOnControlGroup(control_group_id=1)
        else:
            self._preparation = CenterScreenOnMinimap(self.army_rally_minimap_coords)

        return super().prepare()
    
    def notify(self):
        to_remove = None
        if UNIT_UNDER_ATTACK in self._indication_storage.alerts:
            to_remove = UNIT_UNDER_ATTACK
        elif BUILDING_UNDER_ATTACK in self._indication_storage.alerts:
            self._retreating = False
            to_remove = BUILDING_UNDER_ATTACK
        
        if to_remove and not self._retreating:
            self._logger.info('the units have been attacked')
            self._indication_storage.alerts.remove(to_remove)
            self._reinforcer.reinforcing = True
            self._attacking = True

    def get_commands(self, obs):
        queue = Queue()

        if (len(obs.observation.multi_select)) == 0:
            self._attacking = False
            self._reinforcer.reinforcing = False

        if self._attacking:
            roaches = get_seen_units(obs, units.Zerg.Roach)
            if len(roaches) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Roach, control_group_id=1))
            
            ravagers = get_seen_units(obs, units.Zerg.Ravager)
            if len(ravagers) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Ravager, control_group_id=1))

            hydras = get_seen_units(obs, units.Zerg.Hydralisk)
            if len(hydras) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Hydralisk, control_group_id=1))
            
            overseers = get_seen_units(obs, units.Zerg.Overseer)
            if len(overseers) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Overseer, control_group_id=1))
            
            if (len(obs.observation.multi_select)) > 0:
                queue.put(ControlGroupAttackOnMinimap(self._enemy_minimap_coords, control_group_id=1))
                
                if self.burrow:
                    self._control_burrows(obs, queue)
                self._use_ravagers(ravagers, obs, queue)
                self._target_with_hydralisks(obs, queue, hydras)
            
            if self._get_enemy_combat_units_size(obs) > len(obs.observation.multi_select) * 1.4:
                self._logger.info('retreating army to base...')
                queue.put(ControlGroupRetreatOnMinimap(self.army_rally_minimap_coords, control_group_id=1))
                queue.put(ControlGroupRetreatOnMinimap(self.army_rally_minimap_coords, control_group_id=2))
                self.army_size = ceil(self.army_size * 1.4)
                self._reinforcer.reinforcement_size = ceil(self._reinforcer.reinforcement_size * 1.4)
                self._attacking = False
                self._reinforcer.reinforcing = False
                self._retreating = True

            elif self._reinforcer.reinforcement_ready:
                self._logger.info('reinforcing army')
                queue.put(ControlGroupAttackOnMinimap(self._enemy_minimap_coords, control_group_id=2))
                self._reinforcer.reinforcement_ready = False
        else:
            roaches = get_seen_units(obs, units.Zerg.Roach, predicate=lambda roach: roach.health_ratio > 240)
            ravagers = get_seen_units(obs, units.Zerg.Ravager, predicate=lambda ravager: ravager.health_ratio > 240)
            hydras = get_seen_units(obs, units.Zerg.Hydralisk, predicate=lambda ravager: ravager.health_ratio > 240)
            overseers = get_seen_units(obs, units.Zerg.Overseer)
            if len(roaches) > self.army_size:
                self._logger.info('enough roaches to attack')
                self._retreating = False
                queue.put(SetAllAsControlGroup(self.army_rally_minimap_coords, units.Zerg.Roach, control_group_id=1))
                if len(ravagers) > 0:
                    queue.put(AppendAllToControlGroup(units.Zerg.Ravager, control_group_id=1))
                if len(hydras) > 0:
                    queue.put(AppendAllToControlGroup(units.Zerg.Hydralisk, control_group_id=1))
                if len(overseers) > 0:
                    queue.put(AppendAllToControlGroup(units.Zerg.Overseer, control_group_id=1))
                queue.put(ControlGroupAttackOnMinimap(self._enemy_minimap_coords, control_group_id=1))

        self.state = ControllerState.ACTIVE
        return queue
    
    def _control_burrows(self, obs, queue):
        healthy_units = get_seen_units(obs, units.Zerg.RoachBurrowed, predicate=lambda unit: unit.health_ratio > 192)\
            + get_seen_units(obs, units.Zerg.RavagerBurrowed) + get_seen_units(obs, units.Zerg.HydraliskBurrowed)
        for unit in healthy_units:
            unit_coords = unit.x, unit.y
            queue.put(UnburrowUnit(unit.unit_type, unit_coords))
    
        low_units = get_seen_units(obs, units.Zerg.Roach, predicate=lambda unit: unit.health_ratio < 64) # health ratio min is 1, max 255
        for microed_unit in low_units:
            unit_coords = microed_unit.x, microed_unit.y
            queue.put(BurrowUnit(microed_unit.unit_type, unit_coords))


    @staticmethod
    def _calculate_retreat_coords(obs, unit_coords):
        enemy_y, enemy_x = ArmyController._get_enemy_combat_units_coords(obs)
        if len(enemy_y) == 0 or len(enemy_x) == 0:
            return None
    
        new_x, new_y = unit_coords
        if abs(enemy_y.mean() - new_y) > abs(enemy_x.mean() - new_x):
            if enemy_y.mean() > new_y:
                new_y -= RETREAT_RANGE
            else:
                new_y += RETREAT_RANGE
        else:
            if enemy_x.mean() > new_x:
                new_x -= RETREAT_RANGE
            else:
                new_x += RETREAT_RANGE
        
        if new_x < 0:
            new_x = 0
        elif new_x > 83:
            new_x = 83
        
        if new_y < 0:
            new_y = 0
        if new_y > 83:
            new_y = 83

        return new_x, new_y
            
    @staticmethod
    def _get_enemy_combat_units_coords(obs):
       return (
           (obs.observation.feature_screen.unit_type == units.Protoss.Zealot)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Stalker)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Sentry)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Adept)|
           (obs.observation.feature_screen.unit_type == units.Protoss.HighTemplar)|
           (obs.observation.feature_screen.unit_type == units.Protoss.DarkTemplar)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Immortal)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Colossus)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Disruptor)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Archon)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Observer)|
           (obs.observation.feature_screen.unit_type == units.Protoss.WarpPrism)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Phoenix)|
           (obs.observation.feature_screen.unit_type == units.Protoss.VoidRay)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Oracle)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Carrier)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Tempest)|
           (obs.observation.feature_screen.unit_type == units.Protoss.MothershipCore)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Mothership)).nonzero()
        
    
    @staticmethod
    def _get_enemy_combat_units_size(obs):
        return len(get_units(obs, units.Protoss.Zealot)) +\
            len(get_units(obs, units.Protoss.Stalker)) +\
            len(get_units(obs, units.Protoss.Sentry)) +\
            len(get_units(obs, units.Protoss.Adept)) +\
            len(get_units(obs, units.Protoss.HighTemplar)) +\
            len(get_units(obs, units.Protoss.DarkTemplar)) +\
            len(get_units(obs, units.Protoss.Immortal))

    def _target_with_hydralisks(self, obs, queue, hydralisks):
        if len(hydralisks) > 0:
            observers = get_seen_units(obs, units.Protoss.Observer)
            observer = random.choice(observers) if len(observers) > 0 else None
            if observer:
                queue.put(AttackWithUnits(units.Zerg.Hydralisk, attack_coords=(observer.x, observer.y)))
            else:
                immortals = get_seen_units(obs, units.Protoss.Observer)
                immortal = random.choice(immortals) if len(immortals) > 0 else None
                if immortal:
                    queue.put(AttackWithUnits(units.Zerg.Hydralisk, attack_coords=(immortal.x, immortal.y)))
    
    def _use_ravagers(self, ravagers, obs, queue):
        if len(ravagers) > 0:
            enemy_y, enemy_x = self._get_enemy_combat_units_coords(obs)
            if len(enemy_y) > 0 and len(enemy_x) > 0:
                if is_action_available(obs, actions.FUNCTIONS.Effect_CorrosiveBile_screen):
                    idx = random.randint(0, len(enemy_y) - 1)
                    queue.put(CorrosiveBileCommand((enemy_x[idx], enemy_x[idx])))
