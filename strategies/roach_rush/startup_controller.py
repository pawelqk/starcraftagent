from enum import Enum
from functools import partial
from math import ceil, floor
from queue import Queue
from pysc2.lib import units
import random

from helpers import get_units, get_seen_units
from logger import get_logger

from .building_controller import BuildingState, BuildingController
from .controller import ControllerState
from commands import AppendAllToControlGroup, AppendSingleToControlGroup, BuildSpawningPool, BuildExtractor,\
    BuildRoachWarren, BuildEvolutionChamber,\
    CenterScreenOnMinimap, MoveUnit, MoveUnitOnMinimap,\
    FillExtractor, FillMineralFieldFromIdle, InjectLarva,\
    TrainDrone, TrainOverlord, TrainQueen


class StartupController(BuildingController):
    def __init__(self, minimap_coords, indication_storage):
        super().__init__(indication_storage)
        self._minimap_coords = minimap_coords
        self._preparation = CenterScreenOnMinimap(self._minimap_coords)
        self._logger = get_logger('StartupController')
        self._buildings = {
            'Spawning Pool': {
                'cost': 200,
                'type': units.Zerg.SpawningPool,
                'preconditions': lambda _: True,
                'command': BuildSpawningPool,
                'state': BuildingState.NOT_STARTED,
            },
            'Extractor': {
                'cost': 25,
                'type': units.Zerg.Extractor,
                'preconditions': lambda buildings: buildings['Spawning Pool']['state'] != BuildingState.NOT_STARTED,
                'command': BuildExtractor,
                'state': BuildingState.NOT_STARTED,
            },
            'Roach Warren': {
                'cost': 125,
                'type': units.Zerg.RoachWarren,
                'preconditions': lambda buildings: buildings['Spawning Pool']['state'] == BuildingState.DONE,
                'command': BuildRoachWarren,
                'state': BuildingState.NOT_STARTED,
            },
            'Evolution Chamber': {
                'cost': 75,
                'type': units.Zerg.EvolutionChamber,
                'preconditions': lambda buildings: buildings['Roach Warren']['state'] == BuildingState.DONE,
                'command': BuildEvolutionChamber,
                'state': BuildingState.NOT_STARTED,
            },
        }
        self._units = {
            'Drone': {
                'cost': 50,
                'food': 1,
                'type': units.Zerg.Drone,
                'preconditions': lambda _: True,
                'command': TrainDrone,
                'left_to_train': 5
            },
            'Overlord': {
                'cost': 100,
                'food': 0,
                'type': units.Zerg.Overlord,
                'preconditions': self._overlord_preconditions,
                'command': TrainOverlord,
                'left_to_train': 3
            },
            'Queen': {
                'cost': 150,
                'food': 2,
                'type': units.Zerg.Queen,
                'preconditions': lambda buildings: buildings['Spawning Pool']['state'] == BuildingState.DONE,
                'command': TrainQueen,
                'left_to_train': 1
            }
        }

    def get_commands(self, obs) -> Queue:
        self._check_failed(obs)
        self._check_completed(obs)

        self.state = ControllerState.ACTIVE
        minerals, _, food_used, food_cap = self._extract_info(obs)
        queue = Queue()
        for building_name in self._buildings:
            building = self._buildings.get(building_name)
            if minerals >= building['cost'] and building['state'] == BuildingState.NOT_STARTED and building['preconditions'](self._buildings):
                building['state'] = BuildingState.ONGOING
                minerals -= building['cost']
                self._logger.info('enqueueing building of ' + building_name)
                queue.put(building['command'](self._minimap_coords))
        for unit_name in self._units:
            unit = self._units.get(unit_name)
            if unit['left_to_train'] > 0 and unit['preconditions'](self._buildings) and minerals >= unit['cost'] and food_used + unit['food'] <= food_cap:
                unit['left_to_train'] -= 1
                minerals -= unit['cost']
                food_used += unit['food']
                self._logger.info('enqueueing building of ' + unit_name)
                queue.put(unit['command'](self._minimap_coords))

        self._check_done(queue)
        return queue
        
    def _check_done(self, queue):
        if all(self._units.get(unit_name)['left_to_train'] == 0 for unit_name in self._units) and\
            len(self._get_buildings_by_state(BuildingState.DONE)) == len(self._buildings):
            queue.put(AppendSingleToControlGroup(units.Zerg.EvolutionChamber, control_group_id=9))
            self._logger.debug('DONE')
            self.state = ControllerState.DONE
    
    def _overlord_preconditions(self, buildings):
        if self._units['Overlord']['left_to_train'] == 3:
            return True
        else:
            return buildings['Roach Warren']['state'] != BuildingState.NOT_STARTED
