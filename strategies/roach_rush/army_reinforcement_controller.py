from math import floor
import random
from queue import Queue
from .controller import ControllerState, Controller
from commands import AppendAllToControlGroup, CenterScreenOnMinimap,\
    SetAllAsControlGroup, UpgradeToRavager, UpgradeToOverseer
from pysc2.lib import actions, features, units

from logger import get_logger
from helpers import get_seen_units


class ArmyReinforcementController(Controller):
    def __init__(self, army_rally_minimap_coords, indication_storage):
        super().__init__(indication_storage)

        self.reinforcing = False
        self.reinforcement_ready = False
        self.reinforcement_size = 4
        self.army_rally_minimap_coords = army_rally_minimap_coords
        self._logger = get_logger('ArmyReinforcementController')
        self._logger.info('created')
        self._iters = 0
        self._preparation = CenterScreenOnMinimap(self.army_rally_minimap_coords)
   
    def notify(self):
        if self.state == ControllerState.IDLE and self.reinforcing:
            if self._iters == 5:
                self._iters = 0
                self.state = ControllerState.ACTIVE
            else:
                self._iters += 1

    def get_commands(self, obs):
        queue = Queue()
        roaches = get_seen_units(obs, units.Zerg.Roach)
        ravagers = get_seen_units(obs, units.Zerg.Ravager)
        hydras = get_seen_units(obs, units.Zerg.Hydralisk)
        overseers = get_seen_units(obs, units.Zerg.Overseer)
        self._logger.debug('getting commands')
        if len(roaches) > self.reinforcement_size and len(ravagers) > 0:
            self._logger.info('enough roaches to reinforce')
            queue.put(SetAllAsControlGroup(self.army_rally_minimap_coords, units.Zerg.Roach, control_group_id=2))     
            queue.put(AppendAllToControlGroup(units.Zerg.Ravager, control_group_id=2))
            if len(hydras) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Hydralisk, control_group_id=2))
            if len(overseers) > 0:
                queue.put(AppendAllToControlGroup(units.Zerg.Overseer, control_group_id=2))
            self.reinforcement_ready = True
        elif len(roaches) > 0:
            self._logger.debug('checking for ravagers')
            minerals, vespene, food_used, food_cap = self._extract_info(obs)
            if len(ravagers) <= floor(self.reinforcement_size * 0.5) and\
                minerals >= 25 and vespene >= 75 and food_cap - food_used >= 1:
                minerals -= 25
                vespene -= 75
                roach = random.choice(roaches)
                queue.put(UpgradeToRavager((roach.x, roach.y)))
            else:
                self._logger.debug('no ravagers this time')
            
            overlords = get_seen_units(obs, units.Zerg.Overlord)
            self._logger.debug('checking for overseers')
            if len(overlords) > 0 and minerals >= 50 and vespene >= 50:
                overlord = random.choice(overlords)
                queue.put(UpgradeToOverseer((overlord.x, overlord.y)))

        self.state = ControllerState.IDLE
        return queue
                         