from enum import Enum
from queue import Queue

from logger import get_logger

from .controller import Controller

from pysc2.lib import units


BUILDING_COMPLETED = 5
MORPH_COMPLETE = 10
#UPGRADE_COMPLETE = 20


class BuildingState(Enum):
    NOT_STARTED = 0,
    ONGOING = 1,
    DONE = 2


class BuildingController(Controller):
    def __init__(self, indication_storage):
        super().__init__(indication_storage)
        self._logger = get_logger(__name__)
        self._buildings = {}

    def get_commands(self, obs):
        pass
    
    def _check_failed(self, obs):
        if len(self._indication_storage.errors) > 0 and not self._are_any_placeholders(obs):
            for building in self._get_buildings_by_state(BuildingState.ONGOING):
                y, _ = (obs.observation.feature_screen.unit_type == building['type']).nonzero()
                if len(y) == 0:
                    self._logger.warning(str(building['type']) + ' failed. Going to queue him again')
                    building['state'] = BuildingState.NOT_STARTED
                    if len(self._indication_storage.errors) > 0:
                        self._indication_storage.errors.pop()

    def _check_completed(self, obs):
        alert = None
        if BUILDING_COMPLETED in self._indication_storage.alerts:
            alert = BUILDING_COMPLETED
        elif MORPH_COMPLETE in self._indication_storage.alerts:
            alert = MORPH_COMPLETE
        
        if alert:
            for building in self._get_buildings_by_state(BuildingState.ONGOING):
                if not self._is_building_type_ongoing(obs, building['type']):
                    y, _ = (obs.observation.feature_screen.unit_type == building['type']).nonzero()
                    if len(y) == 0:
                        if building['type'] != units.Zerg.Lair:  # TODO: need to distinguish between upgrading and building
                            self._logger.warning(str(building['type']) + ' failed. Going to queue him again')
                            building['state'] = BuildingState.NOT_STARTED
                    else:
                        self._logger.info(str(building['type']) + ' finished')
                        building['state'] = BuildingState.DONE
                        try:
                            self._indication_storage.alerts.remove(alert)
                        except:
                            pass

    def _get_buildings_by_state(self, state):
        return [
            self._buildings.get(building_name) for building_name in self._buildings
            if self._buildings.get(building_name)['state'] == state
        ]
