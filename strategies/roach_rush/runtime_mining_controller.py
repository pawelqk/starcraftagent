from enum import Enum
from functools import partial
from math import ceil, floor
from queue import Queue
from pysc2.lib import units
import random

from helpers import get_seen_units, get_mains
from logger import get_logger

from .controller import Controller, ControllerState
from commands import BuildSpawningPool, BuildExtractor, BuildRoachWarren,\
    CenterScreenOnMinimap, MoveUnit, MoveUnitOnMinimap,\
    FillExtractor, FillMineralFieldFromIdle, InjectLarva,\
    TrainDrone, TrainOverlord, TrainQueen


LARVA_HATCHED = 7


class RuntimeMiningController(Controller):
    def __init__(self, minimap_coords, indication_storage):
        super().__init__(indication_storage)
        self._minimap_coords = minimap_coords
        self._preparation = CenterScreenOnMinimap(self._minimap_coords)
        self._logger = get_logger('RuntimeMiningController' + str(minimap_coords))
        self._main_coords = None
        self._ideal_queen_coords = None
    
    def notify(self):
        if LARVA_HATCHED in self._indication_storage.alerts:
            self._logger.info('larvas were hatched')
            self._indication_storage.alerts.remove(LARVA_HATCHED)
            self.state = ControllerState.ACTIVE

    def get_commands(self, obs):
        if not self._main_coords:
            self._get_required_coords(obs)
        self.state = ControllerState.ACTIVE
        queue = Queue()

        # move queen to posi where it won't overlap larvae
        queen = self._get_queen(obs)
        if queen is not None and self._should_queen_be_moved(queen):
            queue.put(MoveUnit(units.Zerg.Queen, old_coords=(queen.x, queen.y), new_coords=self._ideal_queen_coords))
    
        # inject larvae to main
        if self._get_queen(obs, 25) is not None and self._get_idle_main(obs) is not None:   # works only for single queen-single main scenario for now
            queue.put(InjectLarva(self._minimap_coords))
            self.state = ControllerState.IDLE

        # fill extractors with drones
        for extractor in self._get_unfilled_extractors(obs):
            worker_count = extractor.ideal_harvesters - extractor.assigned_harvesters
            queue.put(FillExtractor(base_coords=self._minimap_coords, worker_count=worker_count))
        
        # move idle drones back to work
        if obs.observation.player.idle_worker_count > 0:
            queue.put(FillMineralFieldFromIdle())
        
        # move idle overlords somewhere else
        for overlord in get_seen_units(obs, units.Zerg.Overlord, lambda overlord: not overlord.active):
            queue.put(MoveUnitOnMinimap(units.Zerg.Overlord,
                screen_coords=(overlord.x, overlord.y), minimap_coords=(random.randint(0, 63), random.randint(0, 63))))

        return queue
    
    def _get_required_coords(self, obs):
        mains = get_mains(obs)
        main = mains[0]
        self._main_coords = (main.x, main.y)
        self._logger.debug('main coords:' + str(self._main_coords))
        self._ideal_queen_coords = self._calculate_queen_coords(obs, self._main_coords)
        self._logger.debug('ideal queen coords:' + str(self._ideal_queen_coords))

    @staticmethod
    def _calculate_queen_coords(obs, main_coords, distance=7):
        main_coords_x, main_coords_y = main_coords

        near_main_x = main_coords_x
        near_main_y = main_coords_y

        minerals_y, minerals_x = ((obs.observation.feature_screen.unit_type == units.Neutral.LabMineralField) |\
            (obs.observation.feature_screen.unit_type == units.Neutral.LabMineralField750)).nonzero()
        rounded_x = minerals_x.mean()
        rounded_y = minerals_y.mean()

        if rounded_x <= main_coords_x:
            near_main_x += distance
        else:
            near_main_x -= distance

        if rounded_y <= main_coords_y:
            near_main_y += distance
        else:
            near_main_y -= distance
    
        return near_main_x, near_main_y
    
    def _should_queen_be_moved(self, queen):
        x, y = self._ideal_queen_coords
        return x != queen.x or y != queen.y

    @staticmethod
    def _get_queen(obs, min_energy=0):
        def energy_predicate(min_energy, queen):
            return queen.energy > min_energy
        
        queens = get_seen_units(obs, units.Zerg.Queen, partial(energy_predicate, min_energy))
        return random.choice(queens) if len(queens) > 0 else None

    @staticmethod
    def _get_unfilled_extractors(obs):
        def unfilled_predicate(extractor):
            return extractor.build_progress == 100 and extractor.assigned_harvesters < extractor.ideal_harvesters
        return get_seen_units(obs, units.Zerg.Extractor, unfilled_predicate)

    @staticmethod
    def _get_idle_main(obs):
        mains = get_mains(obs, predicate=lambda main: main.buff_id_0 != 11 and main.buff_id_1 != 11)
        return random.choice(mains) if len(mains) > 0 else None
