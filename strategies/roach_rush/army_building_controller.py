from queue import Queue
from .controller import ControllerState, Controller
from commands import RecallControlGroupCommand, TrainHydralisks, TrainOverlords, TrainRoaches
from pysc2.lib import actions

from logger import get_logger
from helpers import is_action_available


class ArmyBuildingController(Controller):
    def __init__(self, minimap_coords, mains_group_id, indication_storage):
        super().__init__(indication_storage)
        self.army_rally_minimap_coords = minimap_coords
        self._logger = get_logger(__name__)
        self._mains_group_id = mains_group_id
        self._logger.info('created')
        self._preparation = RecallControlGroupCommand(self._mains_group_id)
        self.produce_hydras = False
        self.produce_overseers = False

        self.hydra = False

    def get_commands(self, obs):
        self.state = ControllerState.ACTIVE
        queue = Queue()
        larvae_count = obs.observation.player.larva_count
        minerals, vespene, food_used, food_cap = self._extract_info(obs)
        food_free = food_cap - food_used

        if larvae_count > 0 and food_free < 5 and minerals >= 100:
            overlord_rally = None
            if self.produce_overseers:
                overlord_rally = self.army_rally_minimap_coords
            queue.put(TrainOverlords(1, self._mains_group_id, overlord_rally))
            minerals -= 100
            larvae_count -= 1

        roach_count = 0
        hydras_count = 0
        while larvae_count > 0 and minerals >= 75 and vespene >= 25 and food_free >= 2:
            if not self.hydra:
                roach_count += 1
                minerals -= 75
                vespene -= 25
                food_free -= 2
                larvae_count -= 1
                if self.produce_hydras:
                    self.hydra = True

            if self.produce_hydras and minerals >= 100 and vespene >= 50 and food_free >= 2:
                hydras_count += 1
                minerals -= 100
                vespene -= 50
                food_free -= 2
                larvae_count -= 1
                self.hydra = False
            else:
                break
        
        if roach_count > 0:
            queue.put(TrainRoaches(roach_count, self._mains_group_id, self.army_rally_minimap_coords))
        if hydras_count > 0:
            queue.put(TrainHydralisks(hydras_count, self._mains_group_id, self.army_rally_minimap_coords))

        return queue
