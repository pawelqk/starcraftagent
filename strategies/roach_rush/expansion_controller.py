from queue import Queue
from .controller import ControllerState, Controller
from commands import AppendAllToControlGroup, BuildExtractor, CenterScreenOnMinimap, Expand,\
    RallyWorkersToMineralsCommand, RecallControlGroupCommand, SetAllAsControlGroup, TrainDrones, TrainQueen
from pysc2.lib import actions, features, units

from logger import get_logger
from helpers import get_seen_units


BUILDING_COMPLETED = 5


class ExpansionController(Controller):
    def __init__(self, from_coords, expansion_coords, indication_storage):
        super().__init__(indication_storage)
        self._from_coords = from_coords
        self.expansion_coords = expansion_coords
        self._expanding = False
        self._expanded = False
        self._building_extractors = False
        self._drones_left = 18
        self._extractors_left = 2
        self._queen_built = False

        self._logger = get_logger('ExpansionController')
        self._logger.info('expansion coords: ' + str(expansion_coords))
    
    def prepare(self):
        if self._expanding or self._building_extractors:
            self._preparation = CenterScreenOnMinimap(self.expansion_coords)
        elif self._expanded:
            self._preparation = RecallControlGroupCommand(6)
        else:
            self._preparation = CenterScreenOnMinimap(self._from_coords)
        
        return super().prepare()

    def notify(self):
        if self.state == ControllerState.IDLE and\
            (BUILDING_COMPLETED in self._indication_storage.alerts or len(self._indication_storage.errors) > 0):
            self._logger.info('back to active')
            self.state = ControllerState.ACTIVE
 
    def get_commands(self, obs):
        queue = Queue()
        if self._expanding:
            self._check_failed(obs)

        self.state = ControllerState.ACTIVE
        minerals, _, food_used, food_cap = self._extract_info(obs)
        if not self._expanding and not self._expanded and not self._building_extractors and food_used > 16 and minerals >= 300:
            self._logger.info('expanding!')
            self._expanding = True
            queue.put(Expand(self.expansion_coords))
            self.state = ControllerState.IDLE
        elif self._expanding:
            y, _ = (obs.observation.feature_screen.unit_type == units.Zerg.Hatchery).nonzero()
            if len(y) > 0 and not self._is_building_type_ongoing(obs, units.Zerg.Hatchery):
                queue.put(SetAllAsControlGroup(self.expansion_coords, units.Zerg.Hatchery, control_group_id=6))
                queue.put(RallyWorkersToMineralsCommand())
                self._expanding = False
                self._expanded = True
                self._logger.info('expansion finished')
        elif self._expanded:
            self._logger.info('training drones for expansion')
            larvae_count = obs.observation.player.larva_count
            food_free = food_cap - food_used
            self.expansion_coords = self._prettify_expansion_coords(obs)
    
            count = 0
            while larvae_count > 0 and minerals >= 25 and food_free >= 1 and self._drones_left > 0:
                count += 1
                minerals -= 25
                food_free -= 1
                larvae_count -= 1
                self._drones_left -= 1
            
            if count > 0:
                queue.put(TrainDrones(count, 6))
            
            if self._drones_left == 0:
                self._expanded = False
                self._building_extractors = True
        elif self._building_extractors:
            if minerals >= 25 and self._extractors_left > 0:
                minerals -= 25
                self._extractors_left -= 1
                queue.put(BuildExtractor(self.expansion_coords))
            
            if not self._queen_built and minerals >= 150 and food_cap - food_used >= 2:
                self._queen_built = True
                queue.put(TrainQueen(self.expansion_coords))
            
            if self._extractors_left == 0 and self._queen_built:
                queue.put(AppendAllToControlGroup(units.Zerg.Hatchery, control_group_id=5))
                self._logger.info('Expanding finished')
                self.state = ControllerState.DONE

        return queue
    
    def _check_failed(self, obs):
        if len(self._indication_storage.errors) > 0 and not self._is_building_type_ongoing(obs, units.Zerg.Hatchery)\
            and not self._are_any_placeholders(obs):
            self._logger.warning('Wrong coords for expanding. Repeating')
            self._expanding = False
            self._indication_storage.errors.pop()

    def _prettify_expansion_coords(self, obs):
        y, x = obs.observation.feature_minimap.selected.nonzero()

        return x.mean(), y.mean()