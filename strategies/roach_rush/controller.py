from enum import Enum
from functools import partial
from math import ceil, floor
from queue import Queue
from pysc2.lib import units
import random

from helpers import get_units, get_seen_units
from logger import get_logger


class ControllerState(Enum):
    ACTIVE = 0,
    READY = 1,
    IDLE = 2,
    DONE = 3


class Controller():
    def __init__(self, indication_storage):
        self._indication_storage = indication_storage
        self.state = ControllerState.ACTIVE
        self._preparation = None
    
    def prepare(self):
        self.state = ControllerState.READY
        return self._preparation
    
    def notify(self):
        pass

    def get_commands(self, obs):
        pass

    @staticmethod
    def _extract_info(obs):
        return (obs.observation.player.minerals,
                obs.observation.player.vespene,
                obs.observation.player.food_used,
                obs.observation.player.food_cap)

    @staticmethod
    def _is_building_type_ongoing(obs, building_type):
        building_y, building_x = (obs.observation.feature_screen.unit_type == building_type).nonzero()
        ongoing_y, ongoing_x = (
            (obs.observation.feature_screen.build_progress > 0) &
            (obs.observation.feature_screen.build_progress < 255)
        ).nonzero()

        for y in building_y:
            for x in building_x:
                if y in ongoing_y and x in ongoing_x:
                    return True
        
        return False

    @staticmethod
    def _are_any_placeholders(obs):
        y, _ = obs.observation.feature_screen.placeholder.nonzero()
        return len(y) > 0
