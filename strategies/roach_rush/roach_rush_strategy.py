from enum import Enum
from functools import partial
from math import ceil, floor
from queue import Queue
import random
from pysc2.lib import units, features

from commands import TrainDrone, TrainOverlord, \
    TrainQueen, BuildRoachWarren, BuildSpawningPool, \
    BuildExtractor, FillExtractor, MoveUnit, AppendAllToControlGroup, ControlGroupAttackOnMinimap, InjectLarva
from logger import get_logger
from helpers import get_seen_units

from .controller import ControllerState
from .startup_controller import StartupController
from .runtime_mining_controller import RuntimeMiningController
from .army_building_controller import ArmyBuildingController
from .army_controller import ArmyController
from .army_reinforcement_controller import ArmyReinforcementController
from .expansion_controller import ExpansionController
from .indication_storage import IndicationStorage
from .tech_controller import TechController
from ..strategy import Strategy
from .upgrade_controller import UpgradeController


class RoachRushStrategy(Strategy):
    def __init__(self):
        self._base_coords = None
        self._enemy_coords = None
        self._army_rally_coords = None
        self.army_done = False

        self._logger = get_logger('RoachRushStrategy')

        self._controllers = {}
        self._queue = Queue()
        self._iterator = None
        self._iterator_key = None
        self._indication_storage = IndicationStorage()

    
    def notify(self, action_results, alerts):
        self._indication_storage.save(action_results, alerts)

    def reset(self):
        self._base_coords = None
        self._enemy_coords = None
        self._army_rally_coords = None
        self.army_done = False

        self._controllers.clear()
        self._queue.queue.clear()
        self._iterator = None
        self._iterator_key = None
        self._indication_storage.clear()

    def create_command(self, obs):
        if not self._base_coords:
            self._base_coords = self._get_base_minimap_coords(obs)
            self._logger.info('base coords acquired: %s, %s' +
                    str(self._base_coords))
            self._enemy_coords = (
                14, 14) if self._base_coords[0] > 32 else (50, 50)
            self._main_coords = self._get_main_coords(obs)
            self._army_rally_coords = self._find_ramp(obs, self._base_coords)
            self._controllers['Startup'] = StartupController(self._base_coords, self._indication_storage)
            self._controllers['RuntimeMiningMain'] = RuntimeMiningController(
                self._base_coords, self._indication_storage)
            self._controllers['Tech'] = TechController(self._base_coords, self._indication_storage)
            self._iterator = iter(self._controllers)
            self._iterator_key = next(self._iterator)
            return AppendAllToControlGroup(units.Zerg.Hatchery, control_group_id=5)

        return self._create_command_roach_rush(obs)

    @staticmethod
    def _find_ramp(obs, start_coords):
        HEIGHT_OFFSET = 12
        height = obs.observation.feature_minimap.height_map
        pathable = obs.observation.feature_minimap.pathable
        height_at_start = height[floor(start_coords[1])][floor(start_coords[0])]

        coords = None
        lowest_dist = 128
        for i in range(64):
            for j in range(64):
                if pathable[i][j] and\
                (height_at_start - HEIGHT_OFFSET) < height[i][j] < height_at_start:
                    dist = abs(start_coords[0] - j) + abs(start_coords[1] - i)
                    if dist < lowest_dist:
                        coords = j, i
                        lowest_dist = dist
        
        return coords

    def _get_main_coords(self, obs):
        mains = [unit for unit in obs.observation.feature_units if
                 unit.unit_type == units.Zerg.Hatchery]
        if len(mains) != 1:
            raise Exception(
                'At the beginning there should only exactly one Hatchery')

        self._logger.info('main coords acquired: %s, %s',
                          mains[0].x, mains[0].y)
        return mains[0].x, mains[0].y

    def _get_base_minimap_coords(self, obs):
        player_y, player_x = (obs.observation.feature_minimap.player_relative ==
                              features.PlayerRelative.SELF).nonzero()

        return player_x.mean(), player_y.mean()

    def _find_expansion_minimap_coords(self, obs):
        creep_y, creep_x = (obs.observation.feature_minimap.creep).nonzero()
        neutral = obs.observation.feature_minimap.player_relative
        if self._base_coords[0] > 32:
            min_creep_y = min(creep_y)
            min_creep_x = min(creep_x)

            limit_x = min_creep_y - 1
            limit_y = min_creep_x - 1
            while limit_x >= 0 and limit_y >= 0:
                point_x = [63, limit_y]
                point_y = [limit_x, 63]
                while point_x[0] != point_y[0] and point_x[1] != point_y[1]:
                    point_x[0] -= 1
                    if neutral[point_x[1]][point_x[0]] == features.PlayerRelative.NEUTRAL:
                        return point_x
                    point_y[1] -= 1
                    if neutral[point_y[1]][point_y[0]] == features.PlayerRelative.NEUTRAL:
                        return point_y

                limit_x -= 1
                limit_y -= 1
        else:     
            max_creep_y = max(creep_y)
            max_creep_x = max(creep_x)

            limit_x = max_creep_x + 1
            limit_y = max_creep_y + 1
            while limit_x < 63 and limit_y < 63:
                point_x = [0, limit_y]
                point_y = [limit_x, 0]
                while point_x[0] != point_y[0] and point_x[1] != point_y[1]:
                    point_x[0] += 1
                    if neutral[point_x[1]][point_x[0]] == features.PlayerRelative.NEUTRAL:
                        return point_x
                    point_y[1] += 1
                    if neutral[point_y[1]][point_y[0]] == features.PlayerRelative.NEUTRAL:
                        return point_y

                limit_x += 1
                limit_y += 1

    @staticmethod
    def _get_roaches(obs):
        return get_seen_units(obs, units.Zerg.Roach)

    def _create_command_roach_rush(self, obs):
        if 'ArmyBuildingController' in self._controllers:
            if self._controllers['Tech'].unit_required is None:
                if self._invisible_detected(obs):
                    unit_to_request = units.Zerg.Overseer
                elif self._air_detected(obs):
                    unit_to_request = units.Zerg.Hydralisk
                
                if unit_to_request and not self._controllers['Tech'].is_tech_done_for_unit(unit_to_request):
                    self._logger.info('going to tech to unit ' + str(unit_to_request))
                    self._controllers['Tech'].unit_required = unit_to_request

            if self._controllers['Tech'].tech_done:
                unit_done = self._controllers['Tech'].get_ready_unit()
                self._logger.info('tech done for' + str(unit_done))
                if unit_done == units.Zerg.Overlord:
                    self._controllers['ArmyBuildingController'].produce_overseers = True
                elif unit_done == units.Zerg.Hydralisk:
                    self._controllers['ArmyBuildingController'].produce_hydras = True
        
        if 'UpgradeController' in self._controllers and self._controllers['UpgradeController'].are_all_upgrades_done_in_building(units.Zerg.RoachWarren):
            self._controllers['ArmyController'].burrow = True

        if self._queue.empty():
            self._controllers[self._iterator_key].notify()

            if self._controllers[self._iterator_key].state == ControllerState.DONE:
                if self._iterator_key == 'Startup':
                    self._controllers['ArmyBuildingController'] = ArmyBuildingController(self._army_rally_coords, 5, self._indication_storage)
                    self._controllers['ArmyReinforcementController'] = ArmyReinforcementController(self._army_rally_coords, self._indication_storage)
                    self._controllers['ArmyController'] = ArmyController(
                        self._army_rally_coords, self._enemy_coords, self._controllers['ArmyReinforcementController'], self._indication_storage)    
                    self._controllers['Expansion'] = ExpansionController(self._base_coords,
                        self._find_expansion_minimap_coords(obs), self._indication_storage)
                elif self._iterator_key == 'Expansion':
                    self._army_rally_coords = self._find_ramp(obs, self._controllers['Expansion'].expansion_coords)
                    self._controllers['RuntimeMiningExpansion'] = RuntimeMiningController(
                        self._controllers['Expansion'].expansion_coords, self._indication_storage)
                    self._controllers['UpgradeController'] = UpgradeController(self._base_coords, 5, 9, self._indication_storage)

                    #self._controllers['ArmyController'].army_rally_minimap_coords = self._army_rally_coords
                    self._controllers['ArmyController'].army_size = 14
                    #self._controllers['ArmyReinforcementController'].army_rally_minimap_coords = self._army_rally_coords
                    self._controllers['ArmyReinforcementController'].reinforcement_size = 8
                    #self._controllers['ArmyBuildingController'].army_rally_minimap_coords = self._army_rally_coords
                    #self._controllers['TechController'] = TechController(self._base_coords, self._units_produced)

                self._controllers.pop(self._iterator_key, None)
                self._iterator_key = self._get_next_key()
            elif self._controllers[self._iterator_key].state == ControllerState.ACTIVE:
                return self._controllers[self._iterator_key].prepare()
            elif self._controllers[self._iterator_key].state == ControllerState.READY:
                self._queue = self._controllers[self._iterator_key].get_commands(obs)

            self._iterator_key = self._get_next_key()
        else:
            return self._queue.get()

    def _get_next_key(self):
        try:
            index = next(self._iterator)
        except (StopIteration, RuntimeError):
            self._iterator = iter(self._controllers)
            index = next(self._iterator)
        finally:
            return index
    
    def _invisible_detected(self, obs):
        y, x = obs.observation.feature_screen.cloaked.nonzero()
        return len(y) != 0 or len(x) != 0

    def _air_detected(self, obs):
        y, x = (
           (obs.observation.feature_screen.unit_type == units.Protoss.Observer)|
           (obs.observation.feature_screen.unit_type == units.Protoss.WarpPrism)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Phoenix)|
           (obs.observation.feature_screen.unit_type == units.Protoss.VoidRay)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Oracle)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Carrier)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Tempest)|
           (obs.observation.feature_screen.unit_type == units.Protoss.MothershipCore)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Mothership)|
           (obs.observation.feature_screen.unit_type == units.Protoss.Stargate)  # for presentation purposes
        ).nonzero()

        return True or len(y) != 0 or len(x) != 0