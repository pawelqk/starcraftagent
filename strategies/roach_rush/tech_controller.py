from queue import Queue
from .controller import ControllerState, Controller
from commands import BuildHydraliskDen, CenterScreenOnMinimap, UpgradeHatcheryToLair
from pysc2.lib import actions, units

from logger import get_logger
from helpers import is_action_available, get_mains
from .building_controller import BuildingController, BuildingState


class TechController(BuildingController):
    def __init__(self, minimap_coords, indication_storage):
        super().__init__(indication_storage)
        self.unit_required = None
        self.tech_done = False
    
        self._minimap_coords = minimap_coords
        self._logger = get_logger(__name__)
        self._indication_storage = indication_storage
        self._preparation = CenterScreenOnMinimap(self._minimap_coords)
        self._requirement = None
        self.state = ControllerState.IDLE

        self._available_units = {
            units.Zerg.Hydralisk: {
                'building': 'Hydralisk Den',
                'done': False
            },
            units.Zerg.Overseer: {
                'building': 'Lair',
                'done': False
            }
        }

        self._buildings = {
            'Hydralisk Den': {
                'minerals': 100,
                'vespene': 100,
                'required': 'Lair',
                'type': units.Zerg.HydraliskDen,
                'command': BuildHydraliskDen,
                'state': BuildingState.NOT_STARTED
            },
            'Lair': {
                'minerals': 150,
                'vespene': 100,
                'required': None,
                'type': units.Zerg.Lair,
                'command': UpgradeHatcheryToLair,
                'state': BuildingState.NOT_STARTED
            }
        }
    
    def notify(self):
        if self.unit_required and self.state == ControllerState.IDLE:
            self.state = ControllerState.ACTIVE
            self._requirements = self._find_requirements()
            self._requirement = self._requirements.pop()

    def get_commands(self, obs):
        queue = Queue()
        if not self._requirement:
            self.tech_done = True
            self.state = ControllerState.IDLE
        else:
            self.state = ControllerState.ACTIVE
            if not (self._requirement['type'] == units.Zerg.Lair or self._requirement['type'] == units.Zerg.Hive):  # failure checking doesnt work for morphing
                self._check_failed(obs)
            self._check_completed(obs)

            if self._requirement['state'] == BuildingState.DONE:
                self._logger.info(str(self._requirement) + ' done')
                self._requirement = self._requirements.pop() if self._requirements else None
            elif self._requirement['state'] == BuildingState.NOT_STARTED:
                minerals, vespene, _, _ = self._extract_info(obs)
                if minerals >= self._requirement['minerals'] and vespene >= self._requirement['vespene'] and self._hack_for_main(obs):
                    self._logger.info(str(self._requirement) + ' set to ongoing')
                    queue.put(self._requirement['command'](self._minimap_coords))
                    self._requirement['state'] = BuildingState.ONGOING

        return queue
    
    def is_tech_done_for_unit(self, unit):
        return self._available_units[unit]['done']

    def get_ready_unit(self):
        unit = self.unit_required
        self._available_units[unit]['done'] = True
        self.tech_done = False
        self.unit_required = None

        return unit

    def _find_requirements(self):
        requirements = []
        root_name = self._available_units[self.unit_required]['building']
        root = self._buildings[root_name]

        while root['state'] != BuildingState.DONE:
            requirements.append(root)
            if not root['required']:
                break

            root_name = root['required']
            root = self._buildings[root_name]
    
        return requirements

    def _hack_for_main(self, obs):  # bizarre :/
        if self._requirement['type'] == units.Zerg.Lair or self._requirement['type'] == units.Zerg.Hive:
            return len(get_mains(obs, predicate=lambda main: main.order_length == 0)) > 0
        
        return True