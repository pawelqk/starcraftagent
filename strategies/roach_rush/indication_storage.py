
class IndicationStorage():
    def __init__(self):
        self.alerts = []
        self.errors = []

    def save(self, action_results, alerts):
        self.errors.extend(action_results)
        self.alerts.extend(alerts)
    
    def clear(self):
        self.alerts.clear()
        self.errors.clear()