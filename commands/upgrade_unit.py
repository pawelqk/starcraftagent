from actions import SelectSingle, MorphRavager, MorphOverseer
from pysc2.lib import actions, units

from .command import Command


class UpgradeToRavager(Command):
    def __init__(self, unit_coords):
        self._unit_coords = unit_coords

    def get_actions(self):
        return [
            SelectSingle(units.Zerg.Roach, self._unit_coords),
            MorphRavager()
        ]


class UpgradeToOverseer(Command):
    def __init__(self, unit_coords):
        self._unit_coords = unit_coords

    def get_actions(self):
        return [
            SelectSingle(units.Zerg.Overlord, self._unit_coords),
            MorphOverseer()
        ]