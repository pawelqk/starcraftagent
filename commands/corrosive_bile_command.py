from actions import CorrosiveBile

from .command import Command


class CorrosiveBileCommand(Command):
    def __init__(self, coords):
        self._coords = coords

    def get_actions(self):
        return [CorrosiveBile(self._coords)]
