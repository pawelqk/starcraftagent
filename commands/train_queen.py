from actions import SelectAllType, TrainUnit
from pysc2.lib import actions, units

from .command import Command


class TrainQueen(Command):
    def __init__(self, main_coords):
        self._base_coords = main_coords

    def get_actions(self):  
        return [
            SelectAllType(units.Zerg.Hatchery),
            TrainUnit(units.Zerg.Hatchery, actions.FUNCTIONS.Train_Queen_quick)
        ]
