import random

from actions import SelectAllType, TrainUnit, RallyUnitOnMinimap, SelectLarvae, RecallControlGroup
from pysc2.lib import actions, units

from .command import Command


class TrainFromLarva(Command):
    def __init__(self, base_coords):
        self._base_coords = base_coords
        self._training_function = None

    def get_actions(self):
        return [
            SelectAllType(units.Zerg.Larva),
            TrainUnit(units.Zerg.Larva, self._training_function)
        ]

class TrainFromMain(Command):
    def __init__(self, training_function, main_id, unit_count, rally_coords):
        self._training_function = training_function
        self._main_id = main_id
        self._unit_count = unit_count
        self._rally_coords = rally_coords

    def get_actions(self):
        return [
            RecallControlGroup(self._main_id),
            SelectLarvae(),
            TrainUnit(units.Zerg.Larva, self._training_function),
            RallyUnitOnMinimap(self._rally_coords)
        ] * self._unit_count


class TrainDrone(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Drone_quick


class TrainDrones(Command):
    def __init__(self, count, main_id):
        self._unit_count = count
        self._main_id = main_id
    
    def get_actions(self):
        return [
            RecallControlGroup(self._main_id),
            SelectLarvae(),
            TrainUnit(units.Zerg.Larva, actions.FUNCTIONS.Train_Drone_quick),
        ] * self._unit_count


class TrainOverlord(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Overlord_quick

    def get_actions(self):
        return super().get_actions() + [RallyUnitOnMinimap((random.randint(0, 63), random.randint(0, 63)))]


class TrainOverlords(TrainFromMain):
    def __init__(self, count, main_id, rally_coords):
        if not rally_coords:
            rally_coords = random.randint(0, 63), random.randint(0, 63)
        super().__init__(actions.FUNCTIONS.Train_Overlord_quick, main_id, count, rally_coords)


class TrainZergling(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Zergling_quick


class TrainRoaches(TrainFromMain):
    def __init__(self, count, main_id, rally_coords):
        super().__init__(actions.FUNCTIONS.Train_Roach_quick, main_id, count, rally_coords)


class TrainHydralisks(TrainFromMain):
    def __init__(self, count, main_id, rally_coords):
        super().__init__(actions.FUNCTIONS.Train_Hydralisk_quick, main_id, count, rally_coords)


class TrainHydralisk(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Hydralisk_quick


class TrainMutalisk(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Mutalisk_quick


class TrainCorruptor(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Corruptor_quick


class TrainInfestor(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Infestor_quick


class TrainSwarmHost(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_SwarmHost_quick


class TrainViper(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Viper_quick


class TrainUltralisk(TrainFromLarva):
    def __init__(self, coords):
        super().__init__(coords)
        self._training_function = actions.FUNCTIONS.Train_Ultralisk_quick
