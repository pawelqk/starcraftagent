import random

from actions import Burrow, Unburrow, SelectSingle
from pysc2.lib import units

from .command import Command


class BurrowUnit(Command):
    def __init__(self, unit_type, coords):
        self._unit_type = unit_type
        self._coords = coords

    def get_actions(self):
        return [
            SelectSingle(self._unit_type, self._coords),
            Burrow()
        ]


class UnburrowUnit(Command):
    def __init__(self, unit_type, coords):
        self._unit_type = unit_type
        self._coords = coords

    def get_actions(self):
        return [
            SelectSingle(self._unit_type, self._coords),
            Unburrow()
        ]
