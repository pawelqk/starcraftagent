from actions import CenterScreenOnSelected, RecallControlGroup, NoOp
from pysc2.lib import actions, units

from .command import Command


class CenterScreenOnControlGroup(Command):
    def __init__(self, control_group_id):
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            RecallControlGroup(self._control_group_id),
            CenterScreenOnSelected(),
            NoOp()
        ]
