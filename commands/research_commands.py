from actions import ResearchBurrow, ResearchGroundArmor, ResearchMissile, ResearchPneumatizedCarapace

from .command import Command


class ResearchBurrowCommand(Command):
    def get_actions(self):
        return [ResearchBurrow()]


class ResearchPneumatizedCarapaceCommand(Command):
    def get_actions(self):
        return [ResearchPneumatizedCarapace()]


class ResearchGroundArmorCommand(Command):
    def get_actions(self):
        return [ResearchGroundArmor()]


class ResearchMissileCommand(Command):
    def get_actions(self):
        return [ResearchMissile()]
