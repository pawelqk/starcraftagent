import random

from actions import SelectRandom, HarvestFromNotFull
from pysc2.lib import units

from .command import Command


class FillExtractor(Command):
    def __init__(self, base_coords, worker_count):
        self._base_coords = base_coords
        self._worker_count = worker_count

    def get_actions(self):
        def mining_drone_predicate(drone):
            # this predicate check if drone is either mining or returning the minerals to base
            # 356 is a raw_ability Harvest_Gather_Drone_quick
            # 360 is a raw_ability Harvest_Return_Drone_quick
            return drone.order_id_0 == 356 or drone.order_id_0 == 360

        return [
            SelectRandom(units.Zerg.Drone, mining_drone_predicate),
            HarvestFromNotFull((units.Zerg.Extractor,))
        ] * self._worker_count
