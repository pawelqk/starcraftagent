import random

from actions import MoveOnScreen, MoveOnMinimap, SelectSingle
from pysc2.lib import units

from .command import Command


class MoveUnit(Command):
    def __init__(self, unit_type, old_coords, new_coords):
        self._unit_type = unit_type
        self._old_coords = old_coords
        self._new_coords = new_coords

    def get_actions(self):
        return [
            SelectSingle(self._unit_type, self._old_coords),
            MoveOnScreen(self._new_coords)
        ]


class MoveUnitOnMinimap(Command):
    def __init__(self, unit_type, screen_coords, minimap_coords):
        self._unit_type = unit_type
        self._screen_coords = screen_coords
        self._minimap_coords = minimap_coords

    def get_actions(self):
        return [
            SelectSingle(self._unit_type, self._screen_coords),
            MoveOnMinimap(self._minimap_coords)
        ]
