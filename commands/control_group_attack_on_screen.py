from actions import AttackOnScreen, RecallControlGroup
from pysc2.lib import actions, units

from .command import Command


class ControlGroupAttackOnScreen(Command):
    def __init__(self, attack_coords, control_group_id):
        self._attack_coords = attack_coords
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            RecallControlGroup(self._control_group_id),
            AttackOnScreen(self._attack_coords)
        ]

