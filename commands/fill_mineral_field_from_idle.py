import random

from actions import SelectIdleWorkers, HarvestFromNotFull
from pysc2.lib import units

from .command import Command
from logger import get_logger


class FillMineralFieldFromIdle(Command):
    def __init__(self):
        self._logger = get_logger(__name__)

    def get_actions(self):
        self._logger.info('going to fill mineral fields with idle drone')
        return [
            SelectIdleWorkers(),
            HarvestFromNotFull((
                units.Neutral.LabMineralField, units.Neutral.LabMineralField750))
        ]
