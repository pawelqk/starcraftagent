from pysc2.lib import units
from actions import SelectRandomFromScreen, ResearchTunnelingClaws,\
    ResearchMuscularAugments, ResearchGroovedSpines, ResearchGlialReconstitution

from .command import Command


class ResearchMuscularAugmentsInHydraliskDen(Command):
    def get_actions(self):
        return [SelectRandomFromScreen(units.Zerg.HydraliskDen), ResearchMuscularAugments()]


class ResearchGroovedSpinesInHydraliskDen(Command):
    def get_actions(self):
        return [SelectRandomFromScreen(units.Zerg.HydraliskDen), ResearchGroovedSpines()]


class ResearchGlialReconstitutionInRoachWarren(Command):
    def get_actions(self):
        return [SelectRandomFromScreen(units.Zerg.RoachWarren), ResearchGlialReconstitution()]


class ResearchTunnelingClawsInRoachWarren(Command):
    def get_actions(self):
        return [SelectRandomFromScreen(units.Zerg.RoachWarren), ResearchTunnelingClaws()]
