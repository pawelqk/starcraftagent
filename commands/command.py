from abc import ABC, abstractmethod


class Command(ABC):
    @abstractmethod
    def get_actions(self):
        pass
