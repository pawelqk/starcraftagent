from actions import SelectAllType, UpgradeToLair
from pysc2.lib import actions, units

from .command import Command


class UpgradeHatcheryToLair(Command):
    def __init__(self, main_coords):
        self._base_coords = main_coords

    def get_actions(self):  
        return [
            SelectAllType(units.Zerg.Hatchery),
            UpgradeToLair()
        ]
