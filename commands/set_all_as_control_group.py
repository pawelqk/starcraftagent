from actions import SelectAllType, SetControlGroup
from pysc2.lib import actions, units

from .command import Command


class SetAllAsControlGroup(Command):
    def __init__(self, base_coords, unit_type, control_group_id):
        self._base_coords = base_coords
        self._unit_type = unit_type
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            SelectAllType(self._unit_type),
            SetControlGroup(self._control_group_id)
        ]
