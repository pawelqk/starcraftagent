from actions import RallyWorkersToMinerals

from .command import Command


class RallyWorkersToMineralsCommand(Command):
    def get_actions(self):
        return [RallyWorkersToMinerals()]
