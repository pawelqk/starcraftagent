from actions import MoveOnScreen, RecallControlGroup
from pysc2.lib import actions, units

from .command import Command


class ControlGroupMoveOnScreen(Command):
    def __init__(self, move_coords, control_group_id):
        self._move_coords = move_coords
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            RecallControlGroup(self._control_group_id),
            MoveOnScreen(self._move_coords)
        ]
