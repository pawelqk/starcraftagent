from actions import AttackOnMinimap, RecallControlGroup
from pysc2.lib import actions, units

from .command import Command


class ControlGroupAttackOnMinimap(Command):
    def __init__(self, attack_coords, control_group_id):
        self._attack_coords = attack_coords
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            RecallControlGroup(self._control_group_id),
            AttackOnMinimap(self._attack_coords)
        ]
