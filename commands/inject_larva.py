from actions import SelectAllType, Inject
from pysc2.lib import actions, units

from .command import Command


class InjectLarva(Command):
    def __init__(self, base_coords):
        self._base_coords = base_coords

    def get_actions(self):
        return [
            SelectAllType(units.Zerg.Queen),  # TODO: with predicate
            Inject(self._base_coords)
        ]
