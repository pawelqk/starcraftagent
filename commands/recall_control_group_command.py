from actions import RecallControlGroup

from .command import Command


class RecallControlGroupCommand(Command):
    def __init__(self, control_group_id):
        self._control_group_id = control_group_id

    def get_actions(self):
        return [RecallControlGroup(self._control_group_id)]
