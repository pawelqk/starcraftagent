from actions import AttackOnScreen, SelectAllType
from pysc2.lib import actions, units

from .command import Command


class AttackWithUnits(Command):
    def __init__(self, unit_type, attack_coords):
        self._unit_type = unit_type
        self._attack_coords = attack_coords

    def get_actions(self):
        return [
            SelectAllType(self._unit_type),
            AttackOnScreen(self._attack_coords)
        ]

