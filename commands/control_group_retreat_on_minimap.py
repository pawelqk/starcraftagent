from actions import MoveOnMinimap, RecallControlGroup
from pysc2.lib import actions, units

from .command import Command


class ControlGroupRetreatOnMinimap(Command):
    def __init__(self, retreat_coords, control_group_id):
        self._retreat_coords = retreat_coords
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            RecallControlGroup(self._control_group_id),
            MoveOnMinimap(self._retreat_coords)
        ]
