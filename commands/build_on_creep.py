from actions import SelectAllType, Build
from pysc2.lib import actions, units

from .command import Command


class BuildOnCreep(Command):
    def __init__(self, build_coords, building_function):
        self._build_coords = build_coords
        self._building_function = building_function

    def get_actions(self):
        return [
            SelectAllType(units.Zerg.Drone),
            Build(self._building_function)
        ]


class BuildSpawningPool(BuildOnCreep):
    def __init__(self, build_coords):
        super().__init__(build_coords, actions.FUNCTIONS.Build_SpawningPool_screen)


class BuildRoachWarren(BuildOnCreep):
    def __init__(self, build_coords):
        super().__init__(build_coords, actions.FUNCTIONS.Build_RoachWarren_screen)


class BuildEvolutionChamber(BuildOnCreep):
    def __init__(self, build_coords):
        super().__init__(build_coords, actions.FUNCTIONS.Build_EvolutionChamber_screen)


class BuildHydraliskDen(BuildOnCreep):
    def __init__(self, build_coords):
        super().__init__(build_coords, actions.FUNCTIONS.Build_HydraliskDen_screen)
