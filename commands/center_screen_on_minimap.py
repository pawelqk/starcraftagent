from actions import CenterScreen, NoOp
from pysc2.lib import actions, units

from .command import Command


class CenterScreenOnMinimap(Command):
    def __init__(self, minimap_coords):
        self._minimap_coords = minimap_coords

    def get_actions(self):
        return [CenterScreen(self._minimap_coords), NoOp()]
