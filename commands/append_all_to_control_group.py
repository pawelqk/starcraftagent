from actions import SelectAllType, SelectRandomFromScreen, AppendToControlGroup
from pysc2.lib import actions, units

from .command import Command


class AppendSingleToControlGroup(Command):
    def __init__(self, unit_type, control_group_id):
        self._unit_type = unit_type
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            SelectRandomFromScreen(self._unit_type),
            AppendToControlGroup(self._control_group_id)
        ]


class AppendAllToControlGroup(Command):
    def __init__(self, unit_type, control_group_id):
        self._unit_type = unit_type
        self._control_group_id = control_group_id

    def get_actions(self):
        return [
            SelectAllType(self._unit_type),
            AppendToControlGroup(self._control_group_id)
        ]
