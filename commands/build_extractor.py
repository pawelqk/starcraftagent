from actions import SelectAllType, BuildOnVespeneGeyser
from pysc2.lib import units

from .command import Command


class BuildExtractor(Command):
    def __init__(self, build_coords):
        self._build_coords = build_coords

    def get_actions(self):
        return [SelectAllType(units.Zerg.Drone), BuildOnVespeneGeyser()]
