from actions import SelectRandomFromScreen, CenterScreen, BuildExpansion, NoOp
from pysc2.lib import actions, units

from .command import Command
from logger import get_logger


class Expand(Command):
    def __init__(self, expansion_coords):
        self._expansion_coords = expansion_coords

    def get_actions(self):
        get_logger('Expand').debug('get_actions')
        return [
            SelectRandomFromScreen(units.Zerg.Drone),
            CenterScreen(self._expansion_coords),
            NoOp(),
            BuildExpansion(self._expansion_coords)
        ]
