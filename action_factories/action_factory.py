from abc import ABC, abstractmethod


class ActionFactory(ABC):
    @abstractmethod
    def reset(self):  # pylint: disable=R0201
        pass

    @abstractmethod
    def get_action(self, _):  # pylint: disable=R0201
        pass
