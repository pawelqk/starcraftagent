
from actions import NoOp
from logger import get_logger


class StrategyBasedActionFactory():
    def __init__(self, strategy):
        self.current_action = None
        self._strategy = strategy
        self._logger = get_logger(__name__)
        self._actions = []
        self._command = None
        self._last_action_succeeded = lambda _: True
    
    def reset(self):
        self._strategy.reset()
        self.__init__(self._strategy)
    
    def get_action(self, obs):
        if self.current_action and not self._last_action_succeeded(obs):
            self._logger.info('last action didnt succeed. Repeating')
            self._logger.debug(self.current_action)
            return self.current_action.get(obs)
        
        self._strategy.notify(obs.observation.action_result, obs.observation.alerts)
        if not self._actions:
            self._command = self._strategy.create_command(obs)
            if not self._command:
                # there could be no command created, it's not an error
                self.current_action = None
                return NoOp().get(obs)
            self._actions = self._command.get_actions()

        self.current_action = self._actions.pop(0)
        action = self.current_action.get(obs)
        if action is None:
            self._logger.error(self.current_action)
    
        self._last_action_succeeded = self.current_action.get_callback()
        return action